class StudentModel:
    def getFavoritePlace(self):
        return self.favoritePlace
    def setFavoritePlace(self,favoritePlace):
        self.favoritePlace = favoritePlace
    def getJoiningYear(self):
        return self.JoiningYear
    def setJoiningYear(self,joiningYear):
        self.JoiningYear = joiningYear
    def getStudentId(self):
        return self.studentId
    def setStudentId(self,studentId):
        self.studentId = studentId
    def getFirstName(self):
        return self.firstName
    def setFirstName(self, firstName):
        self.firstName = firstName
    def getLastname(self):
        return self.lastname
    def setLastname(self,lastname):
        self.lastname = lastname
    def getDateOfBirth(self):
        return self.dateOfBirth
    def setDateOfBirth(self, dateOfBirth):
        self.dateOfBirth = dateOfBirth
    def getMobileNo(self):
        return self.mobileNo
    def setMobileNo(self,mobileNo):
        self.mobileNo = mobileNo
    def getEmail(self):
        return self.email
    def	setEmail(self, email):
        self.email = email
    def getGender(self):
        return self.gender
    def setGender(self, gender):
        self.gender = gender
    def getAddress(self):
        return self.address
    def setAddress(self, address):
        self.address = address
    def getQualification(self):
        return self.Qualification
    def setQualification(self, qualification):
        self.Qualification = qualification
    def getCaste(self):
        return self.caste
    def setCaste(self, caste):
        self.caste = caste
    def	getFees(self):
        return self.fees
    def setFees(self, fees):
        self.fees = fees
    def getCourse(self):
        return self.course

    def setCourse(self, course):
        self.course = course

    def getDue(self):
        return self.due

    def setDue(self, due):
        self.due = due
    def getTotalFee(self):
        return self.totalFee

    def setTotalFee(self, totalFee):
        self.totalFee = totalFee

    def getUserName(self):
        return self.userName

    def setUserName(self, userName):
        self.userName = userName
    def getPassWord(self):
        return self.passWord

    def setPassWord(self, passWord):
        self.passWord = passWord

    def getAdharNumber(self):
        return self.adharNumber

    def setAdharNumber(self, adharNumber):
        self.adharNumber = adharNumber

    def getSecurityQue(self):
        return self.SecurityQue

    def setSecurityQue(self, securityQue):
        self.SecurityQue = securityQue

    def getSerialversionuid(self):
        return self.serialVersionUID

    def getPassword(self):
        return self.PASSWORD

    def getOcFees(self):
        return self.OC_FEES

    def getBcFees(self):
        return self.BC_FEES

    def getScFees(self):
        return self.SC_FEES

    def getStFees(self):
        return self.ST_FEES
