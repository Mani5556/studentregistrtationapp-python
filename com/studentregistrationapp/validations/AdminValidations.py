import re


class AdminValidations:
    #This method is used to validate the username by using isalpha() method
    # returns true if userName is valid else false
    def UserName(self,userName):
        while not userName.isalpha():
            return False
        else:
            return True
    #This method is used for validating the password by using length and search() method in re package
    #returns true if password is valid else false
    def Password(self,password):
        if (len(password) < 8 or len(password) > 12):
            return False
        elif not re.search("[a-z]", password):
            return False
        elif not re.search("[0-9]", password):
            return False
        elif not re.search("[A-Z]", password):
            return False
        elif not re.search("[$#@]", password):
            return False
        elif re.search("\s", password):
            return False
        else:
            return True
