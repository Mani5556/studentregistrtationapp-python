import pickle

from com.studentregistrationapp.models import AdminModel


class AdminDbConnection:
    ADMIN_INFO_FILE = 'C:\\Users\\IMVIZAG\\PycharmProjects\\studentregistrationapp\\com\\studentregistrationapp\\AdminInfoFile'
    UNIQUE_KEY = "GROUP-B"
    #this method saves the object into the file
    def save_object(self,obj, filename):
        with open(filename, 'wb') as output:  # Overwrites any existing file.
            pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)
    #this method reads the object from the file
    def read_object(self,filename) -> object:
        with open(filename,'rb') as input:
            return pickle.load(input)
  #this method register the admin if the user given the correct unique key
    def registerAdmin(self,adminObj):
        if (adminObj.uniqueKey == self.UNIQUE_KEY):
            adminsList = self.read_object(self.ADMIN_INFO_FILE)
            if(adminsList == None):
                adminsList = []
            adminsList.append(adminObj)
            self.save_object(adminsList,self.ADMIN_INFO_FILE)
            return True
        return  False
    #this method logins the admin if the user name and password matches in the database
    def loginAdmin(self,userName,password):
        adminsList = self.read_object(self.ADMIN_INFO_FILE)
        for i in range(0, len(adminsList)):
            if (adminsList[i].userName == userName and adminsList[i].userPassword == password):
                return adminsList[i]
        return None