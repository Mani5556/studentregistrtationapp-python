
from com.studentregistrationapp.dao.AdminDbConnection import AdminDbConnection
from com.studentregistrationapp.models.AdminModel import AdminModel
from com.studentregistrationapp.utilities import Colors
from com.studentregistrationapp.validations.AdminValidations import AdminValidations


class AdminPage:

    UNIQUE_KEY = "GROUP-B"
    adminDbConnection = AdminDbConnection()
    adminValidations = AdminValidations()

    # this method  shows the registration form for the admin
    def adminRegisterForm(self):
        adminModel = AdminModel()
        print( "                            Admin Registration Form                    " )
        print("                   ------------------------------------------")
        adminModel.uniqueKey = input("enter the uniqueKey").strip()

        while True:
            userName = input("enter userName").strip()
            if(self.adminValidations.UserName(userName)):
                adminModel.userName = userName
                break
            else:
                print("Enter the valid userName")

        while True:
             userPassword = input("enter  password").strip()
             if(self.adminValidations.Password(userPassword)):
                 adminModel.userPassword = userPassword
                 break
             else:
                 print(Colors.RED +"invalid password ,password must be >8 letters and atleast one lower,upper,digit and one special Charecter($#@) " + Colors.BLACK)

        while True:
            re_enteredPass = input("re-enter password")
            if(adminModel.userPassword == re_enteredPass ):
               break

        adminModel.securityQues = input("Enter favorite Place")
        if(self.adminDbConnection.registerAdmin(adminModel)):
            print("Admin Registration success")
        else:
             print("registration failed")
        self.adminHomePage()



    #this method shows the reset password page
    def showForgotPassword(self):
        userName = input("enter userName : ")
        favPlace = input("What is your favorite Place")


    #this method checks the admin username and password
    #and if the admin found logins the user

    def adminLoginForm(self):
        isValid = False
        attempts = 0
        print("                         Admin Login Page   ")
        print("               ---------------------------------------")
        while (isValid == False):
            userName = input("enter userName : ").strip()
            passWord = input("enter Password : ").strip()
            admin = self.adminDbConnection.loginAdmin(userName,passWord)
            if(admin != None):
                print("login Success")
                self.adminOperations(admin)
                break
            else:
                attempts += 1
                if(attempts == 3):
                    option = input("you attempted three times with wrong info,DO you want to reset the password(yes/no)").lower()
                    if(option == "yes"):
                       self.showForgotPassword()
                    else:
                        self.adminHomePage()
                        break
                print(Colors.RED +"please enter the valid userName and password" + Colors.BLACK)

 # Application is starts from here
 # this method shoes the home page options
    def adminHomePage(self):

        print(Colors.BLUE +Colors.BOLD + "                               Admin Home Page                          " + Colors.BLACK)
        print("                   --------------------------------------------")
        choice = int(input("1.register \n2.login  \n3.logout\n"))
        adminPage = AdminPage()
        if(choice == 1):
            adminPage.adminRegisterForm()
        if(choice == 2):
            adminPage.adminLoginForm()
        else :
            return
            #HomePage()


    #this method shows the admin Responsibilities

    def adminOperations(self,admin):
        print("                    welcome",admin.userName)
        print("             -----------------------------------------")
        print ("1.Register New Student\n2.Update Student Details\n3.Search Student\n4.Delete Student Details\n5.View StudentMarks\n6.LogOut")
        option = input()