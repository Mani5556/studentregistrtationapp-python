from com.studentregistrationapp.controllers.AdminPage import AdminPage
from com.studentregistrationapp.utilities import Colors


class HomePage:

    print( Colors.BOLD+Colors.GREEN +"                        Student Registration Application" + Colors.BLACK)
    print(Colors.BLUE +"               ---------------------------------------------------" +Colors.BLACK)
    while True:
        try:
           choice = int(input("1.admin \n2.student \n3.exit\n"))
           if(choice < 0 or choice > 3):
               raise Exception()
           break
        except:
            print(Colors.RED + "enter the valid number" + Colors.BLACK)
    if(choice == 1):
        AdminPage().adminHomePage()
    elif(choice == 2):
        print("not implemented")



