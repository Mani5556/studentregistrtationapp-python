R = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

BLACK = '\033[30m'
RED =  '\033[31m'
GREEN = '\033[32m'
BLUE = '\033[34m'